package com.tcbakes.crud.tools.testclasses;

import com.tcbakes.crud.tools.CrudItemsDirectory;
import com.tcbakes.crud.tools.ProvidesBackingForDto;

/**
 * Created by tbaker on 9/15/15.
 */
@ProvidesBackingForDto(Bat.class)
public class _Bat implements BatInterface {


    long widget;
    public long getWidget() {
        return widget;
    }

    public void setWidget(long widget) {
        this.widget = widget;
    }

    @Override
    public int someOtherMethod(int x) {
        return 0;
    }

    @Override
    public void initializeFrom(Bat dto, CrudItemsDirectory dir) {
        this.setWidget(dto.getWidget());
    }
}

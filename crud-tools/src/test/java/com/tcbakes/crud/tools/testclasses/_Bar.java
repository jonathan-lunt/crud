package com.tcbakes.crud.tools.testclasses;

import com.tcbakes.crud.tools.CrudItemsDirectory;
import com.tcbakes.crud.tools.ProvidesBackingForDto;
import com.tcbakes.crud.tools.init.InitializesFrom;

/**
 * Created by tbaker on 9/4/15.
 */
@ProvidesBackingForDto(Bar.class)
public class _Bar implements InitializesFrom<Bar> {
    String beta;

    public String getBeta() {
        return beta;
    }

    public void setBeta(String beta) {
        this.beta = beta;
    }

    @Override
    public void initializeFrom(Bar dto, CrudItemsDirectory dir) {
        beta = dto.getBeta();
    }
}

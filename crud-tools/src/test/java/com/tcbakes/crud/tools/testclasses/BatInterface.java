package com.tcbakes.crud.tools.testclasses;

import com.tcbakes.crud.tools.init.InitializesFrom;

/**
 * Created by tbaker on 9/15/15.
 */
public interface BatInterface extends InitializesFrom<Bat> {

    int someOtherMethod(int x);
}

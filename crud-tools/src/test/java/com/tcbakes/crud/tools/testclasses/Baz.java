package com.tcbakes.crud.tools.testclasses;

import com.tcbakes.crud.tools.CrudItemsDirectory;
import com.tcbakes.crud.tools.InitializesFrom;
import com.tcbakes.crud.tools.ProvidesBackingForDto;

/**
 * Created by tbaker on 9/4/15.
 */
public class Baz {
    String alpha;

    public String getAlpha() {
        return alpha;
    }

    public void setAlpha(String alpha) {
        this.alpha = alpha;
    }
}

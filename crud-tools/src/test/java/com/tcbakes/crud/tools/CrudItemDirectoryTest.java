package com.tcbakes.crud.tools;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import com.tcbakes.crud.tools.testclasses.*;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class CrudItemDirectoryTest {

	@InjectMocks
	CrudItemsDirectory directory;

	@Mock
	ClassResolver mockClassResolver;

    @Mock
    ClassPathScanningCandidateComponentProvider mockScanner;

	@Before
	public void setUpBeforeClass() {

		MockitoAnnotations.initMocks(this);

        when(mockScanner.findCandidateComponents(anyString())).thenAnswer(new Answer<Object>(){
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Set<BeanDefinition> bds = new HashSet<>();

                BeanDefinition bd1 = Mockito.mock(BeanDefinition.class);
                Mockito.when(bd1.getBeanClassName()).thenReturn("com.tcbakes.crud.tools.FakeItem");
                bds.add(bd1);

                BeanDefinition bd2 = Mockito.mock(BeanDefinition.class);
                Mockito.when(bd2.getBeanClassName()).thenReturn("com.tcbakes.crud.tools.FauxItem");
                bds.add(bd2);

                BeanDefinition bd4 = Mockito.mock(BeanDefinition.class);
                Mockito.when(bd4.getBeanClassName()).thenReturn("com.tcbakes.crud.tools.testclasses._Bar");
                bds.add(bd4);

                BeanDefinition bd5 = Mockito.mock(BeanDefinition.class);
                Mockito.when(bd5.getBeanClassName()).thenReturn("com.tcbakes.crud.tools.testclasses._Baz");
                bds.add(bd5);

                BeanDefinition bd6 = Mockito.mock(BeanDefinition.class);
                Mockito.when(bd6.getBeanClassName()).thenReturn("com.tcbakes.crud.tools.testclasses._Foo");
                bds.add(bd6);

                BeanDefinition bd7 = Mockito.mock(BeanDefinition.class);
                Mockito.when(bd7.getBeanClassName()).thenReturn("com.tcbakes.crud.tools.testclasses._Bat");
                bds.add(bd7);

                return bds;
            }
        });

        when(mockClassResolver.resolveClassName(anyString(), any(ClassLoader.class))).thenAnswer(new Answer<Object>() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                String className = ((String) invocation.getArguments()[0]);

                if (className.contains("_Foo"))
                    return _Foo.class;
                else if (className.contains("_Bar"))
                    return _Bar.class;
                else if (className.contains("_Baz"))
                    return _Baz.class;
                else if(className.contains("FakeItem"))
                    return FakeItem.class;
                else if(className.contains("FauxItem"))
                    return FauxItem.class;
                else if(className.contains("_Bat"))
                    return _Bat.class;
                else
                    return null;
            }
        });

	}

	@Test
	public void initializeTest() throws CrudItemRegistrationException {

		final Class<?> clazz = FakeItem.class;

		when(
				mockClassResolver.resolveClassName(anyString(),
						any(ClassLoader.class))).thenAnswer(
				new Answer<Object>() {

					@Override
					public Object answer(InvocationOnMock invocation)
							throws Throwable {
						return clazz;
					}
				});

		
		directory.initialize();
	}
	
	@Test
	public void getInstanceTest() throws InstantiationException, IllegalAccessException, NoBackingForDtoException, CrudItemRegistrationException {
		
		final Class<?> clazz = FakeItem.class;

		when(
				mockClassResolver.resolveClassName(anyString(),
						any(ClassLoader.class))).thenAnswer(
				new Answer<Object>() {

					@Override
					public Object answer(InvocationOnMock invocation)
							throws Throwable {
						return clazz;
					}
				});
		
		directory.initialize();

		FakeItem item = (FakeItem) directory.getBackingInstance(FakeItemDto.class);
		
		Assert.assertNotNull(item);
	}
	
	@Test
    public void getInstanceCustomContextTest() throws InstantiationException, IllegalAccessException, NoBackingForDtoException, CrudItemRegistrationException {
        
        final Class<?> clazz = FauxItem.class;

        when(
                mockClassResolver.resolveClassName(anyString(),
                        any(ClassLoader.class))).thenAnswer(
                new Answer<Object>() {

                    @Override
                    public Object answer(InvocationOnMock invocation)
                            throws Throwable {
                        return clazz;
                    }
                });
        
        directory.initialize();

        FauxItem item = (FauxItem) directory.getBackingInstance("custom", FauxItemDto.class);
        
        Assert.assertNotNull(item);
    }
	
	@Test(expected=CrudItemRegistrationException.class)
	public void initialize_AbstractClassTest() throws CrudItemRegistrationException {
		final Class<?> clazz = AbstractFakeItem.class;

		when(
				mockClassResolver.resolveClassName(anyString(),
						any(ClassLoader.class))).thenAnswer(
				new Answer<Object>() {

					@Override
					public Object answer(InvocationOnMock invocation)
							throws Throwable {
						return clazz;
					}
				});

		directory.initialize();
	}

	@Test(expected=CrudItemRegistrationException.class)
	public void initialize_NoNullaryConstructor() throws CrudItemRegistrationException {
		final Class<?> clazz = PretendItem.class;

		when(
				mockClassResolver.resolveClassName(anyString(),
						any(ClassLoader.class))).thenAnswer(
				new Answer<Object>() {

					@Override
					public Object answer(InvocationOnMock invocation)
							throws Throwable {
						return clazz;
					}
				});
		
		directory.initialize();
	}
	
	@Test(expected=NoBackingForDtoException.class)
	public void getInstance_NotFound() throws CrudItemRegistrationException, InstantiationException, IllegalAccessException, NoBackingForDtoException {
		final Class<?> clazz = FakeItem.class;

		when(
				mockClassResolver.resolveClassName(anyString(),
						any(ClassLoader.class))).thenAnswer(
				new Answer<Object>() {

					@Override
					public Object answer(InvocationOnMock invocation)
							throws Throwable {
						return clazz;
					}
				});
		
		directory.initialize();
		directory.getBackingInstance(PseudoDto.class);
	}

	@Test
	public void getBackingInstanceAndInitializeTest_1() throws CrudItemRegistrationException {
		Foo f = new Foo();
		f.setBar(new Bar());
		f.getBar().setBeta("hello");
		f.setBazes(new ArrayList<Baz>());
		f.getBazes().add(new Baz());
		f.getBazes().get(0).setAlpha("world");
        f.setBat(new Bat());
        f.getBat().setWidget(100L);

		directory.initialize();

		_Foo foo = (_Foo) directory.getBackingInstanceAndInitialize(f);

		Assert.assertEquals("hello", foo.getBar().getBeta());
        Assert.assertEquals("world", foo.getBazes().get(0).getAlpha());
        Assert.assertEquals(100L, foo.getBat().getWidget());

	}

	@Test(expected = NoBackingForDtoException.class)
	public void getBackingInstanceAndInitializeTest_2() throws CrudItemRegistrationException {
		FauxItemDto d = new FauxItemDto();
		d.setId(12345L);

		directory.initialize();

		directory.getBackingInstanceAndInitialize("custom", d);

	}	
}

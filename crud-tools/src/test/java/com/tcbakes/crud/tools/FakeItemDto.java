package com.tcbakes.crud.tools;

import com.tcbakes.crud.spec.base.Dto;

public class FakeItemDto extends Dto<Long>{

	private static final long serialVersionUID = 1L;

	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
	
}

package com.tcbakes.crud.tools;

/**
 * This class is a problem because it does not provide a no-arg constructor
 */
@ProvidesBackingForDto(FakeItemDto.class)
public class PretendItem implements InitializableFrom<FakeItemDto, Long>{

	public PretendItem(String s){
	
	}
	
	@Override
	public void initializeFrom(FakeItemDto dto) {
		
	}

}

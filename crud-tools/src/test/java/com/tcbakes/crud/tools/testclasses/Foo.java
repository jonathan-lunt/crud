package com.tcbakes.crud.tools.testclasses;

import java.util.List;

/**
 * Created by tbaker on 9/4/15.
 */
public class Foo {

    Bar bar;
    List<Baz> bazes;
    Bat bat;

    public Bar getBar() {
        return bar;
    }

    public void setBar(Bar bar) {
        this.bar = bar;
    }

    public List<Baz> getBazes() {
        return bazes;
    }

    public void setBazes(List<Baz> bazes) {
        this.bazes = bazes;
    }


    public Bat getBat() {
        return bat;
    }

    public void setBat(Bat bat) {
        this.bat = bat;
    }
}

package com.tcbakes.crud.tools2.constructedclasses;

import com.tcbakes.crud.tools.ProvidesBackingForDto;

@ProvidesBackingForDto(ConstructedFoo.class)
public class _ConstructedFoo {
	
	Long _id;
	
	public _ConstructedFoo(ConstructedFoo front) {
		_id = front.getId();
	}
	
	public Long getId() {
		return _id;
	}

	public void setId(Long _id) {
		this._id = _id;
	}
}

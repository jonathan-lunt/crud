package com.tcbakes.crud.tools2;

import com.tcbakes.crud.tools.ProvidesBackingForDto;

/**
 * This class is a problem because it does not implement the initializes interface
 */
@ProvidesBackingForDto(FakeItemDto.class)
public class PhonyItem {

}

package com.tcbakes.crud.tools;

import com.tcbakes.crud.spec.base.Dto;

/**
 * Capable of initializing itself from D
 * 
 * @param <D>
 *            The type from which an initialization is supported
 * 
 * @author Tristan Baker
 */
public interface InitializableFrom<D extends Dto<I>, I> {
	/**
	 * Initialize the state of this object from the state of the argument
	 * 
	 * @param dto
	 *            contains the state from which this object will be initialized
	 */
	public void initializeFrom(D dto);
}

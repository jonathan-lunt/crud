package com.tcbakes.crud.impl.base;

import com.tcbakes.crud.spec.base.Dto;

public abstract class AbstractPretendCrudDto extends Dto<Long> {

	private static final long serialVersionUID = 1L;
	
	private FakeManagedCrudDto fakeDto;

	public FakeManagedCrudDto getFakeDto() {
		return fakeDto;
	}

	public void setFakeDto(FakeManagedCrudDto fakeDto) {
		this.fakeDto = fakeDto;
	}

}

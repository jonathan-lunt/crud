package com.tcbakes.crud.impl.base;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.LinkedList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class EntityFactoryTest {

	EntityFactory factory;

	@Mock
	Locator mockLocator;
	@Mock
	EntityManager mockEntityManager;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		
		factory = new EntityFactory(mockLocator);
		
	}

	@Test
	public void buildTest() {

		FakeManagedCrud f = (FakeManagedCrud) factory.build(new FakeManagedCrudDto());

		Assert.assertNotNull(f);
	}
	
	@Test
	public void buildTest_WithItemPreparer() {

		FakeManagedCrud f = (FakeManagedCrud) factory.build(new FakeManagedCrudDto(), new ItemPreparer<FakeManagedCrud, FakeManagedCrudDto, Long>() {

			@Override
			public void prepare(FakeManagedCrud item) {
				item.setSomething(new Object());
				
			}
		});

		Assert.assertNotNull(f);
		Assert.assertNotNull(f.getSomething());
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void buildFromExistingItemTest() {

		when(mockLocator.findById(any(Class.class), any(Long.class)))
				.thenReturn(new FakeManagedCrud());

		FakeManagedCrud f = (FakeManagedCrud) factory.findAndBuild(new Long(1), FakeManagedCrud.class, null);

		Assert.assertNotNull(f);

	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void buildFromExistingItemTest_WithItemPreparere() {

		when(mockLocator.findById(any(Class.class), any(Long.class)))
				.thenReturn(new FakeManagedCrud());

		FakeManagedCrud f = (FakeManagedCrud) factory.findAndBuild(new Long(1), FakeManagedCrud.class, new ItemPreparer<FakeManagedCrud, FakeManagedCrudDto, Long>() {

			@Override
			public void prepare(FakeManagedCrud item) {
				item.setSomething(new Object());
				
			}
		});

		Assert.assertNotNull(f);
		Assert.assertNotNull(f.getSomething());
	}

	@Test
	public void buildFromDtoTest() {

		FakeManagedCrud f = (FakeManagedCrud) factory.build(new FakeManagedCrudDto());

		Assert.assertNotNull(f);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void findAndBuild_nullId() {

		factory.findAndBuild(null, null, null);

	}


	@Test(expected = IllegalArgumentException.class)
	public void build_NullArg() {
		factory.build(null);
	}
	
	@Test
	public void findAllAndBuild() {
		
		List<FakeManagedCrud> list = new LinkedList<>();
		list.add(new FakeManagedCrud());
		list.add(new FakeManagedCrud());
		
		when(mockLocator.findAll(1, 2, FakeManagedCrud.class)).thenReturn(list);
		
		@SuppressWarnings("unchecked")
		List<FakeManagedCrud> cList = factory.findAllAndBuild(1, 2, FakeManagedCrud.class, null);
		
		Assert.assertNotNull(cList);
		Assert.assertEquals(2, cList.size());
		
	}
	
	@Test
	public void findAndBuildSingleFromQuery() {

		Query q = mock(Query.class);

		when(q.getSingleResult()).thenReturn(new FakeManagedCrud());

		FakeManagedCrud c = (FakeManagedCrud) factory.findAndBuildSingleFromQuery(q,
				new ItemPreparer<FakeManagedCrud, FakeManagedCrudDto, Long>() {
					@Override
					public void prepare(FakeManagedCrud item) {
						item.setSomething(new Object());
					}
				});

		Assert.assertNotNull(c);
		Assert.assertNotNull(c.getSomething());
	}
	
	
	@Test
	public void findAndBuildManyFromQuery() {

		Query q = mock(Query.class);

		List<AbstractPretendCrud> list = new LinkedList<>();
		list.add(new PretendCrud());
		list.add(new PretendCrud());

		when(q.getResultList()).thenReturn(list);

		@SuppressWarnings("unchecked")
		List<AbstractPretendCrud> cList = (List<AbstractPretendCrud>) factory
				.findAndBuildManyFromQuery(
						q,
						new ItemPreparer<AbstractPretendCrud, AbstractPretendCrudDto, Long>() {
							@Override
							public void prepare(AbstractPretendCrud item) {
								item.setFake(new FakeManagedCrud());
							}
						});

		Assert.assertNotNull(cList);

		for (AbstractPretendCrud c : cList) {

			Assert.assertNotNull(c.getFake());

		}
	}
	
	@Test
	public void initialize() {
		FakeManagedCrud fakeCrud = new FakeManagedCrud();
		
		factory.initialize(fakeCrud, new ItemPreparer<FakeManagedCrud,FakeManagedCrudDto,Long>() {
			@Override
			public void prepare(FakeManagedCrud item) {
				item.setSomething(new Object());
			}
		});
		
	}
	
	@Test
	public void buildPage_Test(){
		PageOfItems<?, ?, ?> page = factory.buildPage(FakeManagedCrud.class, null);
		
		Assert.assertNotNull(page);
	}

}

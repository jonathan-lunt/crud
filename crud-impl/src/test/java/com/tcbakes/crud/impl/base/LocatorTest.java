package com.tcbakes.crud.impl.base;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.tcbakes.crud.spec.exception.BadRequestException;

public class LocatorTest {

	private static EntityManager entityManager;
	private static Locator locator;
	private static FakeManagedCrud f1;
	private static FakeManagedCrud f2;
	private static FakeManagedCrud f3;

	@BeforeClass
	public static void initialize() {
		EntityManagerFactory emf = Persistence
				.createEntityManagerFactory("unit-test");
		entityManager = emf.createEntityManager();
		locator = new Locator(entityManager);

		entityManager.getTransaction().begin();

		// Add three FakeCrudish records.
		f1 = new FakeManagedCrud();
		f1.setName("fake1");
		entityManager.persist(f1);

		f2 = new FakeManagedCrud();
		f2.setName("fake2");
		entityManager.persist(f2);

		f3 = new FakeManagedCrud();
		f3.setName("fake3");
		entityManager.persist(f3);

		entityManager.getTransaction().commit();
	}

	@Test
	public void getByFieldTest() {

		FakeManagedCrud foundItem = locator.findSingleByFieldValue(
				FakeManagedCrud.class, FakeManagedCrud_.name, f1.getName());

		Assert.assertNotNull(foundItem);
	}

	@Test
	public void getByIdTest() {

		FakeManagedCrud foundItem = locator.findById(FakeManagedCrud.class,
				f1.getId());

		Assert.assertNotNull(foundItem);
	}

	@Test
	public void getMultipleByFieldTest() {

		List<FakeManagedCrud> foundItems = locator.findManyByFieldValue(
				FakeManagedCrud.class, FakeManagedCrud_.name, f1.getName());

		Assert.assertNotNull(foundItems);
	}

	@Test
	public void findAllTest() {
		List<FakeManagedCrud> list = locator.findAll(1, 1,
				FakeManagedCrud.class);

		Assert.assertEquals(1, list.size());
	}
	@Test
	public void findAllTestEmpty() {
		entityManager.getTransaction().begin();

		// Deleting three FakeCrudish records.
		
		entityManager.remove(f1);
		entityManager.remove(f2);
		entityManager.remove(f3);

		entityManager.getTransaction().commit();

				List<FakeManagedCrud> list = locator.findAll(1, 1,
				FakeManagedCrud.class);

		Assert.assertEquals(0, list.size());
		
		entityManager.getTransaction().begin();

		// Adding three FakeCrudish records for other Unit Tests.
		
		f1 = new FakeManagedCrud();
		f1.setName("fake1");
		entityManager.persist(f1);

		f2 = new FakeManagedCrud();
		f2.setName("fake2");
		entityManager.persist(f2);

		f3 = new FakeManagedCrud();
		f3.setName("fake3");
		entityManager.persist(f3);

		entityManager.getTransaction().commit();

	}
	@Test(expected = BadRequestException.class)
	public void findAll_PageNumber0() {

		locator.findAll(0, 1, FakeManagedCrud.class);

	}

	@Test(expected = BadRequestException.class)
	public void findAll_PageNumberOutOfBounds() {

		locator.findAll(4, 1, FakeManagedCrud.class);

	}

	@Test(expected = BadRequestException.class)
	public void findAll_PageSize0() {

		locator.findAll(4, 0, FakeManagedCrud.class);

	}
}

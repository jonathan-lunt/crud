package com.tcbakes.crud.impl.base;

import com.tcbakes.crud.spec.base.Dto;

public class PseudoCrudDto extends Dto<Long> {

	private static final long serialVersionUID = 1L;

	String something;

	public String getSomething() {
		return something;
	}

	public void setSomething(String something) {
		this.something = something;
	}
	
}

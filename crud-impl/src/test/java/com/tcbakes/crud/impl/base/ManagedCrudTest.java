package com.tcbakes.crud.impl.base;

import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.EntityManager;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.tcbakes.crud.impl.access.ACL;
import com.tcbakes.crud.impl.access.User;
import com.tcbakes.crud.impl.exception.CrudItemValidationException;
import com.tcbakes.crud.spec.exception.AccessForbiddenException;
import com.tcbakes.crud.tools.CrudItemsDirectory;

public class ManagedCrudTest {

	@InjectMocks FakeManagedCrud crud;

	@Mock Locator mockLocator;
	@Mock FakeManagedCrud mockFake;
	@Mock EntityManager mockEm;
	@Mock EntityFactory mockEf;
	@Mock TransactionAccessLocal mockTxnAccess;
	@Mock ContextProvider mockContextProvider;
	@Mock CrudItemsDirectory mockDirectory;
	@Mock ExecutionContext mockContext;
	
	User mockActor;
	ACL mockACL;
	ExecutionContext executionContext;

	
	@Before
	public void setup(){
		MockitoAnnotations.initMocks(this);
		
		mockActor = Mockito.mock(User.class);
		Mockito.when(mockActor.createOperationPermitted(Mockito.anyString())).thenReturn(true);
		
		mockACL = Mockito.mock(ACL.class);
		Mockito.when(mockACL.operationPermitted(Mockito.any(User.class), Mockito.anyString())).thenReturn(true);
    	
		crud.setId(new Long(123));
		crud.setAcl(mockACL);

		executionContext = new ExecutionContext();
		
		when(mockContextProvider.getContext()).thenReturn(executionContext);
	
		executionContext.setLocator(mockLocator);
		executionContext.setEm(mockEm);
		executionContext.setTxnAccss(mockTxnAccess);
		executionContext.setItemFactory(mockEf);
		executionContext.setActor(mockActor);	
		
		when(mockEf.getDirectory()).thenReturn(mockDirectory);
		when(mockDirectory.getOperationsByClass(FakeManagedCrud.class)).thenReturn(new HashMap<String, String>(){
            private static final long serialVersionUID = 1L;
        {
	        put("CREATE","CREATE_FAKECRUD");
	    }});
		
	}
	
	@Test
	public void readTest(){
		
		crud.read();
	}
	
	@Test(expected=AccessForbiddenException.class)
	public void read_NotAuthorized_Test(){
		ACL denyingACL = Mockito.mock(ACL.class);
		Mockito.when(denyingACL.operationPermitted(Mockito.any(User.class), Mockito.anyString())).thenReturn(false);
		crud.setAcl(denyingACL);
		
		crud.read();
	}
	
//	@Test(expected=IllegalArgumentException.class)
//	public void read_IllegalArg_Test(){
//		crud.read();
//	}
	
	@Test
	public void createTest(){
		crud.create();
	}
	
	@Test(expected=AccessForbiddenException.class)
	public void create_NotAuthorized_Test(){
		User denyingUser = Mockito.mock(User.class);
		Mockito.when(denyingUser.createOperationPermitted(Mockito.anyString())).thenReturn(false);
		executionContext.setActor(denyingUser);
		crud.create();
	}
	
	@Test
	public void updateTest(){
		FakeManagedCrud newState = new FakeManagedCrud();
		newState.setName("foo");
		newState.setId(new Long(123));
		
		crud.setId(new Long(123));
		crud.update(newState);
	}
	
	@Test(expected=AccessForbiddenException.class)
	public void update_NotAuthorized_Test(){

		ACL denyingACL = Mockito.mock(ACL.class);
		Mockito.when(denyingACL.operationPermitted(Mockito.any(User.class), Mockito.anyString())).thenReturn(false);
		crud.setAcl(denyingACL);
		crud.setId(new Long(123));
		
		FakeManagedCrud newState = new FakeManagedCrud();
		newState.setId(crud.getId());
		

		crud.update(newState);
	}
	
	@Test(expected=CrudItemValidationException.class)
	public void updateTest_InvalidId(){
		FakeManagedCrud newState = new FakeManagedCrud();
		newState.setId(new Long(456));
		
		crud.setId(new Long(123));
		crud.update(newState); 
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void updateTest_NullItem_IllegalArg(){
		crud.update(null);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void updateTest_NullToken_IllegalArg(){
		
		executionContext.setActor(null);
		
		FakeManagedCrud newState = new FakeManagedCrud();
		newState.setId(crud.getId());
		
		crud.update(newState);
	}
	
	@Test
	public void deleteTest(){
		
		crud.delete();
	}
	
	
	@Test(expected=AccessForbiddenException.class)
	public void delete_NotAuthorized_Test(){
		
		ACL denyingACL = Mockito.mock(ACL.class);
		Mockito.when(denyingACL.operationPermitted(Mockito.any(User.class), Mockito.anyString())).thenReturn(false);
		crud.setAcl(denyingACL);
		
		crud.delete();
	}
	
	
	@Test
	public void initializeFromTest(){

		FakeManagedCrudDto dto = new FakeManagedCrudDto();
		
		dto.setId(null);
		dto.setName("hello");
		
		crud.initializeFrom(dto);
		
		Assert.assertEquals(dto.getId(), crud.getId());
		Assert.assertEquals(dto.getName(), crud.getName());
	
	}
	
	@Test
	public void toDtoTest(){

		FakeManagedCrud c = new FakeManagedCrud();
		c.setId(new Long(1));
		c.setName("hello");
		
		FakeManagedCrudDto d = c.toDto();
		
		Assert.assertEquals(c.getId(), d.getId());
		Assert.assertEquals(c.getName(), d.getName());
	
	}
	
	@Test
	public void operationsPermitted_Test(){
		Mockito.when(mockACL.operationsPermitted(Mockito.any(User.class))).thenReturn(new LinkedList<String>());
		
		List<String> permittedOps = crud.operationsPermitted();
		Assert.assertNotNull(permittedOps);
	}
	
	@Test(expected=Exception.class)
	public void operationsPermitted_NoACL_Test(){

		crud.setAcl(null);
		crud.operationsPermitted();
	}

	@Test
	public void updateACL_Test(){
		ACL newState = new ACL();
		
		crud.setId(1676L);
		
		ACL a = crud.updateACL(newState);
		
		Assert.assertEquals(newState.getId(), a.getId());
	}
	
	@Test(expected=Exception.class)
	public void updateACL_NoACL_Test(){
		
		crud.setAcl(null);
		crud.updateACL(new ACL());
		
	}
	
	@Test
	public void readACL_Test(){
		
		ACL a = crud.readACL();
		
		Assert.assertNotNull(a);
	}
	
	@Test(expected=Exception.class)
	public void readACL_NoACL_Test(){
		
		crud.setAcl(null);
		crud.readACL();
		
	}
}

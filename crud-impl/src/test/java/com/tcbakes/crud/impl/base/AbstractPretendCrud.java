package com.tcbakes.crud.impl.base;

public abstract class AbstractPretendCrud extends
		Crud<AbstractPretendCrud, AbstractPretendCrudDto, Long> {

	private static final long serialVersionUID = 1L;
	
	Long id;
	FakeManagedCrud fake;
	
	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public FakeManagedCrud getFake() {
		return fake;
	}

	public void setFake(FakeManagedCrud fake) {
		this.fake = fake;
	}
	

	protected void from(AbstractPretendCrudDto d){
		this.id = d.getId();
		FakeManagedCrud f = (FakeManagedCrud)getContext().getItemFactory().build(d.getFakeDto());
		this.setFake(f);
	}
	
	protected void to(AbstractPretendCrudDto d){
		d.setId(this.getId());
		d.setFakeDto(this.fake.toDto());
	}
}

package com.tcbakes.crud.impl.base;

import com.tcbakes.crud.tools.ProvidesBackingForDto;


@ProvidesBackingForDto(FakeManagedCrudDto.class)
public class FakeManagedCrud extends ManagedCrud<FakeManagedCrud, FakeManagedCrudDto,Long>{

	private static final long serialVersionUID = 1L;

	private Long id;
	private String name;
	
	private Object something;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Object getSomething() {
		return something;
	}

	public void setSomething(Object something) {
		this.something = something;
	}

	@Override
	public void initializeFrom(FakeManagedCrudDto dto) {
		this.id = dto.getId();
		this.name = dto.getName();
	}

	@Override
	public FakeManagedCrudDto toDto() {
		FakeManagedCrudDto dto = new FakeManagedCrudDto();
		dto.setId(this.getId());
		dto.setName(this.getName());
		return dto;
	}

}

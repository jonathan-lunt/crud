package com.tcbakes.crud.impl.base;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

public class PretendCrudTranslationTest {

	@InjectMocks PretendCrud pretendCrud;
	@Mock ContextProvider mockContextProvider;
	@Mock EntityFactory mockFactory;
	ExecutionContext executionContext;

	
	
	@Before
	public void setup(){
		MockitoAnnotations.initMocks(this);
		
		Mockito.when(mockFactory.build(Mockito.anyObject())).thenReturn(new FakeManagedCrud());

		executionContext = new ExecutionContext();
		executionContext.setItemFactory(mockFactory);
		Mockito.when(mockContextProvider.getContext()).thenReturn(executionContext);
	}
	
	@Test
	public void initializeFromTest(){

		
		
		PretendCrudDto d = new PretendCrudDto();

		d.setDescription("A fake description");
		d.setId(new Long(123));
		
		FakeManagedCrudDto f = new FakeManagedCrudDto();
		f.setId(new Long(456));
		f.setName("hello!");

		d.setFakeDto(f);

		pretendCrud.initializeFrom(d);
		
		Assert.assertEquals(d.getDescription(), pretendCrud.getDescription());
		Assert.assertNotNull(pretendCrud.getFake());
		Assert.assertEquals(d.getId(), pretendCrud.getId());
		
	}
	
	
	@Test
	public void translateToTest(){

		PretendCrud c = new PretendCrud();
		c.setId(new Long(123));
		c.setDescription("a pretend description");
		
		FakeManagedCrud f = new FakeManagedCrud();
		f.setName("fake name");
		f.setId(new Long(456));
		
		c.setFake(f);
		
		PretendCrudDto d = (PretendCrudDto) c.toDto();
		
		Assert.assertEquals(c.getDescription(), d.getDescription());
		Assert.assertNotNull(c.getFake());
		Assert.assertEquals(c.getId(), d.getId());
		Assert.assertEquals(c.getFake().getName(), d.getFakeDto().getName());
		
	}
}

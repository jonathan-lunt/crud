package com.tcbakes.crud.impl.base;

import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.tcbakes.crud.impl.access.FakeUser;
import com.tcbakes.crud.impl.access.User;
import com.tcbakes.crud.impl.exception.CrudItemNotFoundException;
import com.tcbakes.crud.impl.exception.CrudItemValidationException;

public class CrudTest {

	@InjectMocks PseudoCrud crud;

	@Mock Locator mockLocator;
	@Mock PseudoCrud mockFake;
	@Mock EntityManager mockEm;
	@Mock EntityFactory mockEf;
	@Mock TransactionAccessLocal mockTxnAccess;
	@Mock ContextProvider mockContextProvider;
	
	ExecutionContext executionContext;
	
	User mockActor = new FakeUser();
	
	@Before
	public void setup(){
		MockitoAnnotations.initMocks(this);
		
		executionContext = new ExecutionContext();
		
		when(mockContextProvider.getContext()).thenReturn(executionContext);
		
		executionContext.setLocator(mockLocator);
		executionContext.setEm(mockEm);
		executionContext.setTxnAccss(mockTxnAccess);
		executionContext.setItemFactory(mockEf);
		executionContext.setActor(mockActor);
		
	}
	
	@Test(expected=CrudItemValidationException.class)
	public void testCollectionUpdate_deleteAndCreateLogicThrowException() {
		
		List<PseudoCrud> oldRuleActions = new ArrayList<PseudoCrud>();
		List<PseudoCrud> newRuleActions = new ArrayList<PseudoCrud>();
		
		PseudoCrud newOne = new PseudoCrud() {
			private static final long serialVersionUID = 1L;

			@Override
			public void doCreateLogic() {
				throw new CrudItemValidationException();
			}
		};
		
		PseudoCrud newTwo = new PseudoCrud();
		
		newRuleActions.add(newOne);
		newRuleActions.add(newTwo);

		crud.collectionUpdate(oldRuleActions, newRuleActions);
	}
	
	@Test(expected=CrudItemValidationException.class)
	public void testCollectionUpdate_updateLogicThrowException() {
		List<PseudoCrud> oldRuleActions = new ArrayList<PseudoCrud>();
		List<PseudoCrud> newRuleActions = new ArrayList<PseudoCrud>();
		
		PseudoCrud oldOne = new PseudoCrud() {
			private static final long serialVersionUID = 1L;

			@Override
			public void doUpdateLogic(PseudoCrud nF) {
				throw new CrudItemValidationException();
			}
		};
		oldOne.setId(3L);
		
		PseudoCrud newOne = new PseudoCrud();
		newOne.setId(3L);

		oldRuleActions.add(oldOne);
		newRuleActions.add(newOne);
		
		crud.collectionUpdate(oldRuleActions, newRuleActions);
	}
	
	@Test(expected=CrudItemNotFoundException.class)
	public void testCollectionUpdate_updateLogicAndCreateWithIdThrowException() {
		List<PseudoCrud> oldRuleActions = new ArrayList<PseudoCrud>();
		List<PseudoCrud> newRuleActions = new ArrayList<PseudoCrud>();
		
		PseudoCrud oldOne = new PseudoCrud();
		
		oldOne.setId(3L);
		
		PseudoCrud newOne = new PseudoCrud();
		newOne.setId(4L); // Id doesn't match.

		oldRuleActions.add(oldOne);
		newRuleActions.add(newOne);
		
		crud.collectionUpdate(oldRuleActions, newRuleActions);
	}
	
	@Test
	public void testCollectionUpdate() {
		List<PseudoCrud> oldRuleActions = new ArrayList<PseudoCrud>();
		List<PseudoCrud> newRuleActions = new ArrayList<PseudoCrud>();
		
		// Old state
		PseudoCrud oldOne = spy(new PseudoCrud());
		oldOne.setId(1L);
		
		PseudoCrud oldTwo = spy(new PseudoCrud());
		oldTwo.setId(2L);

		PseudoCrud oldThree = spy(new PseudoCrud());
		oldThree.setId(3L);
		oldThree.setSomething("name1");
		
		oldRuleActions.add(oldOne);
		oldRuleActions.add(oldTwo);
		oldRuleActions.add(oldThree);
		
		// New state
		PseudoCrud newOne = spy(new PseudoCrud());
		newOne.setId(3L);
		newOne.setSomething("name2");
		PseudoCrud newTwo = spy(new PseudoCrud());
		PseudoCrud newThree = spy(new PseudoCrud());
		
		newRuleActions.add(newOne);
		newRuleActions.add(newTwo);
		newRuleActions.add(newThree);
		
		// Perform difference logic on the two collections
		crud.collectionUpdate(oldRuleActions, newRuleActions);
		
		// Deletes
		verify(oldOne, times(1)).doDeleteLogic();
		verify(oldTwo, times(1)).doDeleteLogic();
		
		// Updates
		verify(oldThree, times(1)).doUpdateLogic(newOne);
		
		// Creates
		verify(newTwo, times(1)).doCreateLogic();
		verify(newThree, times(1)).doCreateLogic();
	}
	
}

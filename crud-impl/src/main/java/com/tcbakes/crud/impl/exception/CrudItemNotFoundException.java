package com.tcbakes.crud.impl.exception;

public class CrudItemNotFoundException extends AbstractException {

	private static final long serialVersionUID = 1L;

	public CrudItemNotFoundException() {
		super();
	}
	
	public CrudItemNotFoundException(String idAsString, Class<?> entityClass) {
		super(String.format("Could not find %1$s with id %2$s", entityClass.getSimpleName(), idAsString));
	}

}

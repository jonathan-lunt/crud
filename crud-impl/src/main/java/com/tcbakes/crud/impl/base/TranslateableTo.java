package com.tcbakes.crud.impl.base;

import com.tcbakes.crud.spec.base.Dto;

/**
 * Capable of translating itself to D
 * 
 * @param <D>
 *            The target type of the translation
 * 
 * @author Tristan Baker
 */
public interface TranslateableTo<D extends Dto<I>, I> {

	/**
	 * Translate to the target type
	 * 
	 * @return The initilized target type
	 */
	public D toDto();
}

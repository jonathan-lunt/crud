package com.tcbakes.crud.impl.access;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.tcbakes.crud.impl.base.Crud;
import com.tcbakes.crud.spec.access.ACLDto;
import com.tcbakes.crud.spec.exception.AccessForbiddenException;
import com.tcbakes.crud.spec.exception.BadRequestException;
import com.tcbakes.crud.tools.ProvidesBackingForDto;

@ProvidesBackingForDto(ACLDto.class)
public class ACL extends Crud<ACL, ACLDto, Long> {

    private static final long serialVersionUID = 1L;

    private Long id;

    private User owningUser;

    private List<UserGroup> groups;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getOwningUser() {
        return owningUser;
    }

    public void setOwningUser(User user) {
        this.owningUser = user;
    }

    public List<UserGroup> getGroups() {
        return groups;
    }

    public void setGroups(List<UserGroup> groups) {
        this.groups = groups;
    }

    @Override
    public void doReadLogic() {
        super.doReadLogic();

        User actor = getContext().getActor();

        if (!operationPermitted(actor, GroupOperation.READ.toString()))
            throw new AccessForbiddenException(
                    MessageFormat
                            .format("User ''{0}'' does not have permission to perform operation ''{1}'' on this ACL",
                                    actor.getId(),
                                    GroupOperation.READ.toString()));
    }

    @Override
    public void doUpdateLogic(ACL newState) {
        super.doUpdateLogic(newState);

        User actor = getContext().getActor();

        if (!operationPermitted(actor, GroupOperation.UPDATE.toString()))
            throw new AccessForbiddenException(
                    MessageFormat
                            .format("User ''{0}'' does not have permission to perform operation ''{1}'' on this ACL",
                                    actor.getId(),
                                    GroupOperation.UPDATE.toString()));
    }

    @Override
    public void initializeFrom(ACLDto dto) {

        this.setId(dto.getId());

        if (dto.getOwningUserId() == null) {
            throw new BadRequestException(String.format(
                    "Owning user id cannot be null. ACL id='%s'", this.getId()));
        }

        User user = getContext().getLocator().findById(User.class,
                dto.getOwningUserId());

        if (user != null) {
            this.setOwningUser(user);
        }

        if (dto.getUserGroups() != null) {
			this.setGroups(new ArrayList<UserGroup>());

            // Remove duplicates from Groups
            HashSet<String> usergroupSet = new HashSet<String>();
            usergroupSet.addAll(dto.getUserGroups());
            
			for (String gpdto : usergroupSet) {
				UserGroup gp = getContext().getLocator().findSingleByFieldValue(
						UserGroup.class, UserGroup_.name, gpdto);

				if (gp != null) {
					getContext().getItemFactory().initialize(gp, null);
					this.getGroups().add(gp);
				}
			}
		}
    }

    @Override
    public ACLDto toDto() {
        ACLDto dto = new ACLDto();

        dto.setId(this.getId());

        if (this.getOwningUser() != null) {
            dto.setOwningUserId(this.getOwningUser().getId());
        }

        if (this.getGroups() != null) {
            dto.setUserGroups(new ArrayList<String>());

            for (UserGroup gp : this.getGroups()) {
                dto.getUserGroups().add(gp.getName());
            }
        }

        return dto;
    }

    /**
     * Determines whether or not an actor can perform an operation.
     * <p>
     * The policy for determining whether or not a user can do something is as
     * follows:
     * <ol>
     * <li>If the actor is the owner, all operations are permitted</li>
     * <li>If the actor is not the owner, then the set of allowed operations is
     * determined as follows:</li>
     * <ol>
     * <li>A set is created that represents the intersection of (1) the set of
     * all the groups that the actor is a member of and (2) the set of groups
     * referenced by this ACL</li>
     * <li>A further set is created that represents the intersection of all
     * operations referenced by the groups in the set from step 2.1</li>
     * <li>If the operation parameter is contained in the set from step 2.2,
     * then the operation is allowed, else the operation is not allowed</li>
     * </ol>
     * </ol>
     * </p>
     * 
     * @param actor
     *            The user attempting to perform an operation
     * @param operation
     *            The operation to perform
     * @return true if the operation is allowed, false otherwise.
     */
    public boolean operationPermitted(User actor, String operation) {

        if (actor == null)
            throw new IllegalArgumentException("actor cannot be null");

        if (StringUtils.isEmpty(operation))
            throw new IllegalArgumentException(
                    "operation cannot be null or empty");
        
        if (this.getOwningUser().getId().equals(actor.getId()))
            return true;

        List<String> allPermittedOperations = operationsPermitted(actor);

        return allPermittedOperations.contains(GroupOperation.ALL.toString())
                || allPermittedOperations.contains(operation);

    }

    /**
     * Retrieves the list of operations available for the specified user based
     * on matching UserGroups between the User and the ACL.
     * <p>
     * The list of operations can be one of the following:
     * <ol>
     * <li>{ALL}: If the user is the owning user of the ACL, or</li>
     * <li>The intersection of operations between a matching group between the
     * user and the ACL
     * </ol>
     * </p>
     * 
     * @param actor
     *            The User
     * @return A list of permitted operations for this user and ACL. If no
     *         operations are permitted, then the list will be empty
     */
    public List<String> operationsPermitted(User actor) {

        if (actor == null)
            throw new IllegalArgumentException("actor cannot be null");

        List<String> permittedOperations = new ArrayList<String>();

        if (this.getOwningUser().getId().equals(actor.getId())) {
            permittedOperations.add(GroupOperation.ALL.toString());
        } else {
            for (UserGroup g : actor.getUserGroups()) {
                for (UserGroup gAcl : this.getGroups()) {
                    if (g.getName().equals(gAcl.getName())) {
                        for (String op : gAcl.getOperations()) {
                            if (!permittedOperations.contains(op)) {
                                permittedOperations.add(op);
                            }
                        }
                    }
                }
            }
        }

        return permittedOperations;
	}
	
	
}

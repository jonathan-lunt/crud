package com.tcbakes.crud.impl.base;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

@Stateless
public class TransactionAccess implements TransactionAccessLocal {

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void requiresNew(TransactionWrappedLogic logic) {
		logic.execute();
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.MANDATORY)
	public void mandotory(TransactionWrappedLogic logic) {
		logic.execute();
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.NEVER)
	public void never(TransactionWrappedLogic logic) {
		logic.execute();
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void notSupported(TransactionWrappedLogic logic) {
		logic.execute();
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void required(TransactionWrappedLogic logic) {
		logic.execute();
	}

}

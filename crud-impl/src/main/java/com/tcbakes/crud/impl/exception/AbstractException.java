package com.tcbakes.crud.impl.exception;

import javax.ejb.ApplicationException;

@ApplicationException(inherited = true, rollback = true)
public abstract class AbstractException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public AbstractException() {
		super();
	}
	
	public AbstractException(String msg) {
		super(msg);
	}

	public AbstractException(String msg, Throwable cause) {
		super(msg, cause);
	}
}

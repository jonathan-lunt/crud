package com.tcbakes.crud.impl.base;

import java.io.Serializable;
import java.util.List;

import com.tcbakes.crud.impl.access.User;
import com.tcbakes.crud.impl.exception.CrudItemNotFoundException;
import com.tcbakes.crud.impl.exception.CrudItemValidationException;
import com.tcbakes.crud.impl.exception.ValidationFailures;
import com.tcbakes.crud.spec.base.Dto;
import com.tcbakes.crud.spec.exception.NotFoundException;
import com.tcbakes.crud.tools.InitializableFrom;

/**
 * Supports [C]reate, [R]ead, [U]pdate and [D]elete behavior.
 * 
 * <p>
 * Extending classes will have access to:
 * 
 * <ul>
 * <li>an {@link javax.persistence.EntityManager} for managing the persistent
 * state of this object or any other item in the sytem</li>
 * <li>a {@link Locator} for easy construction of simple queries</li>
 * <li>a {@link TransactionAccessLocal} for controlling transaction scope</li>
 * <li>a {@link EntityFactory} for creating equivalent instances of Crud items
 * given a Dto</li>
 * </ul>
 * 
 * 
 * @param <C>
 *            The explicit Crud type
 * @param <D>
 *            The DTO type to and from which this object is Translateable
 * @param <I>
 *            The type used as the primary identifier
 * 
 * @author Tristan Baker
 */
public abstract class Crud<C extends Crud<C, D, I>, D extends Dto<I>, I> extends Item
        implements InitializableFrom<D, I>, TranslateableTo<D, I>, Serializable {

    private static final long serialVersionUID = 1L;
    
    /**
     * Default constructor
     */
    public Crud() {
    }

    /**
     * @return The primary identifier for this type
     */
    public abstract I getId();

    /**
     * @param id
     *            The identifier to set
     */
    public abstract void setId(I id);

	/**
	 * Performs any necessary activities associated with reading this object
	 * 
	 * @param actor
	 *            The user performing the operation
	 */
	public void read() {

		doReadLogic();
	}

    /**
     * Performs business logic that should occur whenever this item is
     * retrieved. This method is called during {@link Crud#read(User)}
     * after the item has been retrieved from the system and this object's state
     * is fully initialized.
     * <p>
     * Any exception thrown during execution of this method will cause the
     * entire operation to be rolled back
     * </p>
     * 
     */
    public void doReadLogic() {

    }

	/**
	 * Persists the new state of this object to the persistent store. The id of
	 * the newState must match the id of this object.
	 * 
	 * @param newState
	 *            The new state of this object
	 * @throws CrudItemValidationException
	 *             If the newState is invalid based validation rules.
	 */
	public void update(C newState)
			throws CrudItemValidationException {

		if (newState == null)
			throw new IllegalArgumentException("newState cannot be null");

		doUpdateLogic(newState);

		getContext().getEm().merge(newState);
	}
    
    /**
     * Performs business logic that should occur whenever this item is updated.
     * This method is called during {@link Crud#update(Crud, User)} after
     * basic validation is performed but before this object is updated to
     * reflect the newState and persisted to the system.
     * <p>
     * If further modifications need to be performed, they should be performed
     * directly on the newState parameter. i.e, by the end of this method,
     * newState should be exactly what you want persisted to the system.
     * </p>
     * <p>
     * Any exception thrown during execution of this method will cause the
     * entire operation to be rolled back
     * </p>
     * 
     * @param newState
     *            The proposed new state of item.
     * @param actor
     *            The user performing the operation
     */
    public void doUpdateLogic(C newState) {
    	
    	if(newState == null)
    		throw new IllegalArgumentException("newState must be provided");
    	
    	
    	if (!this.getId().equals(newState.getId())) {
			ValidationFailures f = new ValidationFailures("id",
					"id cannot be modified");
			throw new CrudItemValidationException(f);
		}
    }

	/**
	 * Removes this object from the persistent store.
	 * 
	 */
	public void delete() {

		doDeleteLogic();
		getContext().getEm().remove(this);
	}
	
    /**
     * Performs business logic that should occur when this item is deleted. This
     * method is called during {@link Crud#delete(User)} prior to the
     * item being removed from the system.
     * <p>
     * Any exception thrown during execution of this method will cause the
     * entire operation to be rolled back
     * </p>
     * @param actor
     *            The user performing the operation
     */
    public void doDeleteLogic() {

    }

    
	/**
	 * Performs creation business logic and then persists this object to
	 * persistent store.
	 * 
	 */
	public void create() {

		doCreateLogic();
		getContext().getEm().persist(this);

	}
	
    /**
     * Performs business logic that should occur whenever this item is persisted
     * to to this system. This method is called during
     * {@link Crud#create(User)} prior to persisting the item to the
     * system
     * <p>
     * Any exception thrown during execution of this method will cause the
     * entire operation to be rolled back
     * </p>
     * 
     */
    public void doCreateLogic() {

    }

    /**
     * Initializes the entire state of this object from the state
     * of the provided dto.
     * 
     * @param dto
     */
    @Override
    public abstract void initializeFrom(D dto);

    /**
     * Translates the entire state of this object to an
     * equivalent {@link Dto} representation.
     * 
     */
    @Override
    public abstract D toDto();

    /**
     * Determines which CRUD items in a collection, as part of an update, should
     * be created, updated, or deleted (doCreateLogic, doUpdateLogic, delete).
     * 
     * @param oldState
     *            A list of older states
     * @param newState
     *            A list of newer states
     * @param actor
     *            The user performing the operation
     * @throws NotFoundException
     *             If the item identified by id is not found or d contains a
     *             reference to another item that is not found
     */
    public <X extends Crud<X, Y, Z>, Y extends Dto<Z>, Z> void collectionUpdate(
            List<X> oldState, List<X> newState) {

        if (oldState != null && newState != null) {

            // Check for updates/deletes
            for (X crud : oldState) {
                X newStateMatching = listContains(newState, crud);

                if (newStateMatching != null) {
                    crud.doUpdateLogic(newStateMatching);
                } else {
                    if (getContext().getItemFactory() != null) {
                    	getContext().getItemFactory().initialize(crud, null);
                    }
                    crud.doDeleteLogic();
                    getContext().getEm().remove(crud);
                }
            }

            // Check for new additions
            for (X crud : newState) {
                X oldStateMatching = listContains(oldState, crud);

                if (oldStateMatching == null && crud.getId() == null) {
                    crud.doCreateLogic();
                } else if (oldStateMatching == null && crud.getId() != null) {
                    // TODO: Output Dto class name, not entity class name
                    throw new CrudItemNotFoundException(
                            crud.getId().toString(), crud.getClass());
                }
            }
        }
    }

    /**
     * Locate a CRUD item by id within a specified list.
     * 
     * @param list
     *            List to check contents of
     * @param item
     *            Item to compare with
     * @return The item if found, null otherwise
     */
    private static <X extends Crud<X, Y, Z>, Y extends Dto<Z>, Z> X listContains(
            List<X> list, X item) {
        for (X c : list) {
            if (c.getId() != null && item != null
                    && c.getId().equals(item.getId())) {
                return c;
            }
        }

        return null;
    }

}

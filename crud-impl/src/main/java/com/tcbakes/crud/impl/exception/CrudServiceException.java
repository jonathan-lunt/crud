package com.tcbakes.crud.impl.exception;

public class CrudServiceException extends AbstractException {

	private static final long serialVersionUID = 1L;

	public CrudServiceException(){
	}
	
	public CrudServiceException(String msg){
		super(msg);
	}
	
	public CrudServiceException(String msg, Throwable cause){
		super(msg, cause);
	}
}

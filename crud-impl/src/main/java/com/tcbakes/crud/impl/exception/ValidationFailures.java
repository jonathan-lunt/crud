package com.tcbakes.crud.impl.exception;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class ValidationFailures implements Serializable {

	private static final long serialVersionUID = 1L;

	public ValidationFailures(){
		
	}
	
	public ValidationFailures(String field, String validationErrorReason) {
		addValidationError(field, validationErrorReason);
	}
	
	Map<String, List<String>> fieldErrors = new HashMap<String, List<String>>();
	boolean conflicts = false;
	
	public boolean isConflicts() {
        return conflicts;
    }

    public void setConflicts(boolean conflicts) {
        this.conflicts = conflicts;
    }

    public Map<String, List<String>> getFieldErrors() {
		return fieldErrors;
	}

	public void setFieldErrors(Map<String, List<String>> fieldErrors) {
		this.fieldErrors = fieldErrors;
	}
	
	public void addValidationError(String field, String validationErrorReason) {

		if (!fieldErrors.containsKey(field)) {
			LinkedList<String> list = new LinkedList<String>();
			fieldErrors.put(field, list);
		}

		fieldErrors.get(field).add(validationErrorReason);
	}
	
	public void mergeValidationErrors(ValidationFailures moreFailures){
		if(moreFailures.hasFailures()){
			for(Map.Entry<String, List<String>> error : moreFailures.getFieldErrors().entrySet()){
				if(fieldErrors.containsKey(error.getKey())) {
					fieldErrors.get(error.getKey()).addAll(error.getValue());
				} else {
					fieldErrors.put(error.getKey(), error.getValue());
				}
			}
		}
	}
	
	public boolean hasFailures(){
		return !fieldErrors.isEmpty();
	}	
}

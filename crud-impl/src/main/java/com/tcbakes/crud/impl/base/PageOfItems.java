package com.tcbakes.crud.impl.base;

import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tcbakes.crud.impl.access.User;
import com.tcbakes.crud.spec.base.Dto;
import com.tcbakes.crud.spec.exception.AccessForbiddenException;
import com.tcbakes.crud.spec.paging.PageOfItemsDto;

/**
 * Represents a subset of the total number of accessible items of a specific
 * type available in the system
 * 
 * @param <C>
 *            The Crud item
 * @param <D>
 *            The equivalent Dto item
 * @param <I>
 *            The unique identifier for the item
 * 
 * @author Tristan Baker
 */
public class PageOfItems<C extends Crud<C, D, I>, D extends Dto<I>, I> {

	private static Logger logger = LoggerFactory.getLogger(PageOfItems.class);

	private int thisPageNumber;
	private int totalNumberOfPages;
	private int numberOfItemsThisPage;
	private List<C> pageItems;

	private EntityFactory factory;
	private ItemPreparer<C, D, I> preparer;
	private Class<C> managedClass;
	private Locator locator;

	/**
	 * @return The current page number
	 */
	public int getThisPageNumber() {
		return thisPageNumber;
	}

	public void setThisPageNumber(int n) {
		this.thisPageNumber = n;
	}

	/**
	 * @return The total number of pages of this size in the system
	 */
	public int getTotalNumberOfPages() {
		return totalNumberOfPages;
	}

	public void setTotalNumberOfPages(int n) {
		this.totalNumberOfPages = n;
	}

	/**
	 * @return The total number of records on this page
	 */
	public int getNumberOfItemsThisPage() {
		return numberOfItemsThisPage;
	}

	public void setNumberOfItemsThisPage(int n) {
		this.numberOfItemsThisPage = n;
	}

	/**
	 * @return The list of records on this page
	 */
	public List<C> getPageItems() {
		return pageItems;
	}

	public void setPageItems(List<C> items) {
		this.pageItems = items;
	}

	/**
	 * @return A factory that can be used to find and build system items
	 */
	protected EntityFactory getFactory() {
		return this.factory;
	}

	public void setFactory(EntityFactory f) {
		this.factory = f;
	}

	/**
	 * @return The Crud item class managed by this page
	 */
	protected Class<C> getManagedClass() {
		return managedClass;
	}

	public void setManagedClass(Class<C> managedClass) {
		this.managedClass = managedClass;
	}

	/**
	 * The {@link ItemPreparer} will be delegated to prior to calling the item's
	 * {@link Crud#read(User)} method
	 * 
	 * @return An optional preparer that must be used to prepare an item before
	 *         it can be asked to perform an action.
	 */
	protected ItemPreparer<C, D, I> getPreparer() {
		return preparer;
	}

	public void setPreparer(ItemPreparer<C, D, I> preparer) {
		this.preparer = preparer;
	}

	protected Locator getLocator() {
		return locator;
	}

	public void setLocator(Locator locator) {
		this.locator = locator;
	}

	/**
	 * Process the page of records. This includes iterating through the list of
	 * items and in turn, delegating read logic to each of the items. If any
	 * item indicates (via an UnauthorizedAccessException) that the user does
	 * not have permission to read the item, it will be removed from this page
	 * of records. Note that this means the page may contain fewer records at
	 * the conclusion of the execution of this method
	 * 
	 * @param pageNum
	 *            The page to retrieve
	 * @param pageSize
	 *            The size of the page
	 */
	@SuppressWarnings("unchecked")
	public void read(int pageNum, int pageSize) {

		List<C> finalPage = new LinkedList<C>();

		for (C item : (List<C>) factory.findAllAndBuild(pageNum, pageSize,
				getManagedClass(), getPreparer())) {

			if (item != null) {
				String itemType = item.getClass().getName();
				I id = item.getId();

				if (preparer != null)
					preparer.prepare(item);

				try {
					item.doReadLogic();
					finalPage.add(item);
				} catch (AccessForbiddenException e) {
					// TODO: Removing a record will shrink the size of the page
					// which will mean that the user's request will appear not
					// to be honored. Need to figure out if/how to address this
					logger.debug(
							"Removing item from page as the user is not authorized to see it. itemType={}, id={}",
							itemType, id);
				}
			}

		}

		int totalRecords = locator.count(getManagedClass());

		this.thisPageNumber = pageNum;
		this.numberOfItemsThisPage = finalPage.size();
		this.totalNumberOfPages = totalRecords % pageSize == 0 ? (totalRecords / pageSize)
				: (totalRecords / pageSize) + 1;
		this.pageItems = finalPage;
	}

	/**
	 * @return An equivalent {@link PageOfItemsDto}
	 */
	public PageOfItemsDto<D> toDto() {

		PageOfItemsDto<D> por = new PageOfItemsDto<>();
		por.setTotalNumberOfPages(this.totalNumberOfPages);
		por.setNumberOfItemsThisPage(this.getNumberOfItemsThisPage());
		por.setThisPageNumber(this.getThisPageNumber());

		por.setPageItems(new LinkedList<D>());

		for (C dto : this.getPageItems()) {
			por.getPageItems().add(dto.toDto());
		}

		return por;
	}
}

package com.tcbakes.crud.impl.access;

import java.util.Calendar;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(AccessToken.class)
public abstract class AccessToken_ {

	public static volatile SingularAttribute<AccessToken, String> id;
	public static volatile SingularAttribute<AccessToken, Boolean> enabled;
	public static volatile SingularAttribute<AccessToken, Calendar> mostRecentlyActive;
	public static volatile SingularAttribute<AccessToken, Calendar> start;
	public static volatile SingularAttribute<AccessToken, User> user;
	public static volatile SingularAttribute<AccessToken, Integer> allowableInactiveSeconds;

}


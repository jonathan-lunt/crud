package com.tcbakes.crud.impl.base;

import com.tcbakes.crud.impl.exception.ValidationFailures;

/**
 * A validation behavior that detects when the object is invalid according to some implementation specific rules.
 *
 * @since 1.9.14
 */
public interface Validates {

    /**
     * Validate the state of this object against the validation rules defined by the object.
     * <p/>
     * It is expected that this validation method propagate the validation throughout the object hierarchy, deferring
     * further validation to the further validate methods defined by other {@link Validates} objects referenced by this
     * object.  In the event that there is a cycle in the object graph being validated, the validationContext can be
     * used to detect when a node in the graph has been re-visited so that the propagation can stop and the validation
     * propagation is able to eventually complete.
     * <p/>
     * For example, let's assume the following object instance exists (using JSON notation for clarity/simplicity)
     * <pre>
{
  "propertyX": "1",
  "propertyY": {
    "stuff": [{"a": 1}, {"a": 4}, {"a": -40}]
  }
}
     * </pre>
     *
     * The validation is kicked off by calling validate on the outermost object, passing in "" as the validationContext.
     * The outermost object validates its scalar propertyX by running the value "1" through whatever appropriate
     * validation logic is defined.  It then propagates the validation to the "propertyY" object by calling validate and
     * passing in "propertyY." as the value.  This strategy of appending the name of the property to an ever growing
     * validationContext allows for the production of very useful error messages in the ValidationFailures object.  For
     * instance, let's say that it was illegal to have "a" values &lt;0 in the "stuff" array.  Appropriately appending to
     * and passing along the validationContext value means that something like the following ValidationFailures object
     * could be produced:
     * <pre>
{
  "fieldErrors":{"propertyY.stuff[2].a": ["Must be greater than 0"]}
}
     * </pre>
     *
     * If the the nature of the class design dictates that cycles are possible in the object graph, then the
     * validationContext can be designed so that unique object IDs are present in the validationContext and can be
     * examined within each validation method to determine if the object has previously been validated.
     *
     * @param validationContext A context value intended to track the absolute location of the validation execution as
     *                          this method propagates its validation to any referenced objects that are also
     *                          validate-able.
     * @return
     */
    ValidationFailures validate(String validationContext);
}

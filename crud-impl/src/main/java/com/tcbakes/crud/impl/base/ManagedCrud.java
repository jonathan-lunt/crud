package com.tcbakes.crud.impl.base;

import java.text.MessageFormat;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.tcbakes.crud.impl.access.ACL;
import com.tcbakes.crud.impl.access.GroupOperation;
import com.tcbakes.crud.impl.access.User;
import com.tcbakes.crud.impl.access.UserGroup;
import com.tcbakes.crud.impl.exception.CrudItemValidationException;
import com.tcbakes.crud.impl.exception.CrudServiceException;
import com.tcbakes.crud.impl.exception.ValidationFailures;
import com.tcbakes.crud.spec.base.Dto;
import com.tcbakes.crud.spec.exception.AccessForbiddenException;
import com.tcbakes.crud.tools.CrudItemsDirectory;

public abstract class ManagedCrud<C extends ManagedCrud<C, D, I>, D extends Dto<I>, I>
		extends Crud<C, D, I> {

	
	private static final long serialVersionUID = 1L;

	private ACL acl;

	public ACL getAcl() {
		return acl;
	}

	public void setAcl(ACL acl) {
		this.acl = acl;
	}

	/**
	 * @return The canonical permission name that signifies the ability to
	 *         create an item of this type
	 */
	protected String getCreateOperationName() {
	    CrudItemsDirectory d = this.getContext().getItemFactory().getDirectory();
	    return d.getOperationsByClass(this.getClass()).get(GroupOperation.CREATEPREFIX.toString());
	}

	/**
	 * Assigns the the default ACL for this item, then checks to make sure that
	 * the actor performing the operation has permission to do so.
	 * 
	 * @see com.tcbakes.crud.impl.base.Crud#doCreateLogic(com.tcbakes.crud.impl.access.User)
	 */
	@Override
	public void doCreateLogic() {
		super.doCreateLogic();

		User actor = getContext().getActor();
		
		// Create the Default ACL for this item
		ACL acl = new ACL();
		getContext().getItemFactory().initialize(acl, null);
		acl.setOwningUser(actor);
		this.setAcl(acl);

		if (!permittedOperation(getCreateOperationName()))
			throw new AccessForbiddenException(
					MessageFormat
							.format("User ''{1}'' does not have permission to perform operation ''{2}'' on ''{0}''",
									this.getClass().getSimpleName(),
									actor.getId(), getCreateOperationName()));
	}

	/**
	 * Checks to make sure that the actor performing the operation has
	 * permission to do so
	 * 
	 * @see com.tcbakes.crud.impl.base.Crud#doReadLogic(com.tcbakes.crud.impl.access.User)
	 */
	@Override
	public void doReadLogic() {
		super.doReadLogic();

		User actor = getContext().getActor();
		
		
		if (!permittedOperation(GroupOperation.READ.toString()))
			throw new AccessForbiddenException(
					MessageFormat
							.format("User ''{1}'' does not have permission to perform operation ''{2}'' on ''{0}'' with id ''{3}''",
									this.getClass().getSimpleName(),
									actor.getId(),
									GroupOperation.READ.toString(),
									this.getId()));
	}


	/**
	 * Checks to make sure that the actor performing the operation has
	 * permission to do so
	 * 
	 * @param mockActor
	 *            The User on whose behalf the operation is performed
	 * @see com.tcbakes.crud.impl.base.Crud#doUpdateLogic(com.tcbakes.crud.impl.base.Crud,
	 *      com.tcbakes.crud.impl.access.User)
	 */
	@Override
	public void doUpdateLogic(C newState) {
		super.doUpdateLogic(newState);
		
		User actor = getContext().getActor();
		
		
		if (!permittedOperation(GroupOperation.UPDATE.toString()))
			throw new AccessForbiddenException(
					MessageFormat
							.format("User ''{1}'' does not have permission to perform operation ''{2}'' on ''{0}'' with id ''{3}''",
									this.getClass().getSimpleName(),
									actor.getId(),
									GroupOperation.UPDATE.toString(),
									this.getId()));

	}

	/**
	 * Checks to make sure that the actor performing the operation has
	 * permission to do so
	 * 
	 * @see com.tcbakes.crud.impl.base.Crud#doDeleteLogic(com.tcbakes.crud.impl.access.User)
	 */
	@Override
	public void doDeleteLogic() {
		super.doDeleteLogic();
		
		User actor = getContext().getActor();
		
		if (!permittedOperation(GroupOperation.DELETE.toString()))
			throw new AccessForbiddenException(
					MessageFormat
							.format("User ''{1}'' does not have permission to perform operation ''{2}'' on ''{0}'' with id ''{3}''",
									this.getClass().getSimpleName(),
									actor.getId(),
									GroupOperation.DELETE.toString(),
									this.getId()));
	}

	/**
	 * Determines whether a {@link User} is capable of performing the specified
	 * operation on this CRUD item. In order for a user to be allowed to perform
	 * the operation, the following conditions must be met:
	 * 
	 * <ol>
	 * <li>The {@link ACL} associated with this item is not null and the
	 * operation to be performed is permitted by the ACL, OR
	 * <li>The CREATE_ operation for this item (used when creating this item)
	 * exists within the user's {@link UserGroup}s</li>
	 * </ol>
	 * 
	 * @param mockActor
	 *            The {@link User} performing the operation
	 * @param operation
	 *            The operation to perform
	 */
	public boolean permittedOperation(String operation) {

		User actor = getContext().getActor();
		
		if (actor == null) {
			throw new IllegalArgumentException("The current context does not contain an actor");
		}

		if (StringUtils.isEmpty(operation)) {
			throw new IllegalArgumentException("operation cannot be empty");
		}

		if (operation.startsWith("CREATE_")) {
			return actor.createOperationPermitted(operation);
		} else {
			
			if( this.getAcl() != null)
			{
				return this.getAcl().operationPermitted(actor, operation);
			
			}
			
			return false;
		}

	}

	/**
	 * Returns a list of all the operations a user is capable of performing on
	 * this item.
	 * 
	 * @param mockActor
	 *            The user for which the returned operations are allowed
	 * @return A list of operations allowed to be performed by the actor. If no
	 *         operations are allowed, an empty list is returned. If all
	 *         operations are allowed, the a list with a single "ALL" entry is
	 *         returned
	 * @see GroupOperation#ALL
	 */
	public List<String> operationsPermitted() {
		
		User actor = getContext().getActor();
		
		if (actor == null) {
			throw new IllegalArgumentException("The current context does not contain an actor");
		}
		
		if (this.getAcl() == null)
			throw new CrudServiceException(
					MessageFormat.format(
							"Item ''{0}'' with id ''{1}''is missing an ACL and one is expected",
							this.getClass().getName(), this.getId()));
		
	
	    return this.getAcl().operationsPermitted(actor);
	}
	
	/**
	 * Updates the ACL for this item
	 * @param newState the desired newState of the ACL
	 * @param mockActor the user on whose behalf this update is being performed
	 */
	public ACL updateACL(ACL newState){
		
		User actor = getContext().getActor();
		
		if (actor == null) {
			throw new IllegalArgumentException("The current context does not contain an actor");
		}
		
		if (this.getAcl() == null) {
			throw new CrudServiceException(
					MessageFormat.format(
							"Item ''{0}'' with id ''{1}''is missing an ACL and one is expected",
							this.getClass().getName(), this.getId()));
		}
		
		if (this.getAcl().getOwningUser() != null &&
		        !this.getAcl().getOwningUser().equals(newState.getOwningUser())) {
            ValidationFailures f = new ValidationFailures("owningUserId",
                    "id cannot be modified");
            throw new CrudItemValidationException(f);
        }
		
		newState.setId(this.getAcl().getId());
		this.getAcl().update(newState);
		return newState;
	}
	
	public ACL readACL(){
		
		User actor = getContext().getActor();
		
		if (actor == null) {
			throw new IllegalArgumentException("The current context does not contain an actor");
		}
		
		if (this.getAcl() == null)
			throw new CrudServiceException(
					MessageFormat.format(
							"Item ''{0}'' with id ''{1}''is missing an ACL and one is expected",
							this.getClass().getName(), this.getId()));
		
		return this.getAcl();
	}
}

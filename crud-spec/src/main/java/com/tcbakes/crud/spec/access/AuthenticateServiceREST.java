package com.tcbakes.crud.spec.access;

import javax.ws.rs.Consumes;
import javax.ws.rs.CookieParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.tcbakes.crud.spec.exception.BadRequestException;
import com.tcbakes.crud.spec.exception.NotFoundException;
import com.tcbakes.crud.spec.exception.ServiceException;
import com.tcbakes.crud.spec.exception.ValidationException;

/**
 * Authenticates a {@link CredentialDto}
 * 
 * @author Tristan Baker
 */
@Path("/")
@Produces(MediaType.APPLICATION_JSON)
public interface AuthenticateServiceREST {

    /**
     * Authenticates the provided credential. If the credentials are valid, an
     * {@link AccessTokenDto} is produced so that it can be provided to other
     * services and used to validate that the user granted the token is allowed
     * to perform a particular service operation.
     * 
     * @param cred
     *            The credential to authenticate
     * @return An access token usable with other services.
     * @throws ValidationException
     *             If the credential provided is not valid
     */
    @POST
    @Path("auth")
    @Consumes(MediaType.APPLICATION_JSON)
    public AccessTokenDto authenticate(CredentialDto cred)
            throws ValidationException;

    /**
     * Logs out the user by disabling the token Id.
     * 
     * @param tokenId
     *            Unique string identifying the login user
     * @return none
     * @throws BadRequestException
     *             if the tokenId is null NotFoundException if there is no
     *             associated entry to the tokenId ServiceException for all
     *             other internal errors
     */
    @POST
    @Path("logout")
    public void logout(@CookieParam("tokenId") String token)
            throws BadRequestException, NotFoundException, ServiceException;
}

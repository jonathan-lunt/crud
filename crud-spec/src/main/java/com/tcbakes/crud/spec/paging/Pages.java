package com.tcbakes.crud.spec.paging;

import com.tcbakes.crud.spec.base.Dto;
import com.tcbakes.crud.spec.base.Reads;
import com.tcbakes.crud.spec.exception.AccessForbiddenException;

/**
 * A specification for paging of a set of records. Note that Paging does not fit
 * nicely into a standard RESTful CRUD service. This means that there is no
 * natural unambiquous HTTP URL pattern that will nicely capture a paging
 * request. If this interface is used in conjunction with {@link Reads}, jax-rs
 * implementations won't be able to disambiguate the requests since both Read
 * and Page use "GET /"
 * 
 * @param <T>
 *            The item being paged
 * 
 * @author Tristan Baker
 */
public interface Pages<T extends Dto<?>> {

	public PageOfItemsDto<T> getPage(Integer pageNum, Integer pageSize,
			String tokenId) throws AccessForbiddenException;

}

package com.tcbakes.crud.spec.access;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonTypeInfo;

/**
 * A Credential provides enough detail such that the system's authentication
 * mechanism can validate the identity of the user providing the credential and
 * grant the appropriate level of access to the system
 * 
 * @author Tristan Baker
 * 
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY, property = "class")
public abstract class CredentialDto implements Serializable {

	private static final long serialVersionUID = 1L;

}

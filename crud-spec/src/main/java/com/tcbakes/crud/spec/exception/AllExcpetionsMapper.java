package com.tcbakes.crud.spec.exception;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.codehaus.jackson.map.ObjectMapper;

@Provider
public class AllExcpetionsMapper implements ExceptionMapper<AbstractSpecException> {

    private static ObjectMapper mapper = new ObjectMapper();
    
	public Response toResponse(AbstractSpecException exception) {
        try {
            return Response.status(exception.getHttpStatusCode())
                    .type(MediaType.APPLICATION_JSON)
                    .entity(exceptionAsJson(exception))
                    .build();
        } catch (Exception e) {
           return Response.status(exception.getHttpStatusCode())
           .type(MediaType.APPLICATION_JSON)
           .entity(exception.getMessage())
           .build();
        }
	}
	
	public static String exceptionAsJson(AbstractSpecException ex) {
		try {
			return mapper.writeValueAsString(ex.toDetail());
		} catch(Exception e){
			return "{\"message\": \"problem creating error message\"}";
		}
	}

}

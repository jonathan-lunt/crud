package com.tcbakes.crud.spec.access;

import java.util.HashMap;

public class UserGroupDetailsDto extends UserGroupDto {

    private static final long serialVersionUID = 1L;

    /*
    * A list of usernames of the members of this group
    */
    private HashMap<String, Long> members;

    public HashMap<String, Long> getMembers() {
        return members;
    }

    public void setMembers(HashMap<String, Long> members) {
        this.members = members;
    }
}

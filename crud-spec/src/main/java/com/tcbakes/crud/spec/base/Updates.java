package com.tcbakes.crud.spec.base;

import com.tcbakes.crud.spec.access.AuthenticateServiceREST;
import com.tcbakes.crud.spec.exception.BadRequestException;
import com.tcbakes.crud.spec.exception.NotFoundException;
import com.tcbakes.crud.spec.exception.ServiceException;
import com.tcbakes.crud.spec.exception.AccessForbiddenException;
import com.tcbakes.crud.spec.exception.ValidationException;

/**
 * Defines a generic contract for updating the state of a system item
 * 
 * @param <D> The {@link Dto} being managed 
 * @param <I> The type of the {@link Dto}'s primary id.
 * 
 * @author Tristan Baker
 */
public interface Updates<D extends Dto<I>, I> {

	/**
	 * Updates the state of the item.  Access is granted/denied via
	 * the tokenId, which identifies the actor performing the operation.
	 * 
	 * @param id
	 *            The primary id of the item to update
	 * @param d
	 *            The new state for the item
	 * @param tokenId
	 *            The token granting access to perform the update
	 * @return The state updated item
	 * @throws BadRequestException
	 *             If id or d are null
	 * @throws NotFoundException
	 *             If the item identified by id is not found or d references
	 *             another item that is not found
	 * @throws ValidationException
	 *             If d is not valid as defined by system validation rules
	 * @throws AccessForbiddenException
	 *             If the AccessToken referenced by tokenId does not grant
	 *             access to this operation
	 * @throws ServiceException
	 *             All other errors
	 * @see AuthenticateServiceREST#authenticate(com.tcbakes.crud.spec.access.CredentialDto)
	 */

	public abstract D update(I id, D d, String tokenId) throws BadRequestException,
			NotFoundException, ValidationException,
			AccessForbiddenException, ServiceException;
}

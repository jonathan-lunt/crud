package com.tcbakes.crud.spec.paging;

import java.io.Serializable;
import java.util.List;


public class PageOfItemsDto<T> implements Serializable {

	private static final long serialVersionUID = 1L;

	private int thisPageNumber;
	
    private int totalNumberOfPages;
	
    private int numberOfItemsThisPage;
	
    private List<T> pageItems;

	public int getThisPageNumber() {
		return thisPageNumber;
	}

	public void setThisPageNumber(int n) {
		this.thisPageNumber = n;
	}

	public int getTotalNumberOfPages() {
		return totalNumberOfPages;
	}

	public void setTotalNumberOfPages(int n) {
		this.totalNumberOfPages = n;
	}

	public int getNumberOfItemsThisPage() {
		return numberOfItemsThisPage;
	}

	public void setNumberOfItemsThisPage(int n) {
		this.numberOfItemsThisPage = n;
	}

	public List<T> getPageItems() {
		return pageItems;
	}

	public void setPageItems(List<T> items) {
		this.pageItems = items;
	}
}

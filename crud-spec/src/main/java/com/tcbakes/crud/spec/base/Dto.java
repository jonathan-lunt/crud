package com.tcbakes.crud.spec.base;

import java.io.Serializable;

public abstract class Dto<I> implements Serializable{


	private static final long serialVersionUID = 1L;

	I id;

	public I getId() {
		return id;
	}

	public void setId(I id) {
		this.id = id;
	}
}

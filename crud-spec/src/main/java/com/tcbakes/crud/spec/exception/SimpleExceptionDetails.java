package com.tcbakes.crud.spec.exception;

public class SimpleExceptionDetails {
    private String errorMessage;

    public SimpleExceptionDetails(){
    	
    }
    
    public SimpleExceptionDetails(String error) {
        this.errorMessage = error;
    }
    
    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String error) {
        this.errorMessage = error;
    }
}

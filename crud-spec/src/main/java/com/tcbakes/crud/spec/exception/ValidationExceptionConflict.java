package com.tcbakes.crud.spec.exception;

import java.util.List;
import java.util.Map;

import javax.ws.rs.core.Response.Status;

public class ValidationExceptionConflict extends ValidationException {
    private static final long serialVersionUID = 1L;
    
    public ValidationExceptionConflict() {
        super();
    }
    
    public ValidationExceptionConflict(String msg, Map<String, List<String>> fieldErrors) {
        super(msg, fieldErrors);
    }

    @Override
    public Status getHttpStatusCode() {
        return Status.CONFLICT;
    }
}

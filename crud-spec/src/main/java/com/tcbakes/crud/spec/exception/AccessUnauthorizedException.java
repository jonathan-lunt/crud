package com.tcbakes.crud.spec.exception;

import javax.ws.rs.core.Response.Status;

/**
 * The request requires user authentication, which has not yet occurred.
 * A request to authenticate followed by a retry of this request my help.
 * 
 * @see javax.ws.rs.core.Response.Status#UNAUTHORIZED
 * @author Tristan Baker
 */
public class AccessUnauthorizedException extends AbstractSpecException {

    private static final long serialVersionUID = 1L;

    public AccessUnauthorizedException() {
    }

    public AccessUnauthorizedException(String msg) {
        super(msg);
    }

    public AccessUnauthorizedException(String msg, Throwable cause) {
        super(msg, cause);
    }

    @Override
    public Status getHttpStatusCode() {
        return Status.UNAUTHORIZED;
    }
    
}

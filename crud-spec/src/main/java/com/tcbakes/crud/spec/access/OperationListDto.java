package com.tcbakes.crud.spec.access;

import java.io.Serializable;
import java.util.List;

public class OperationListDto implements Serializable {

    private static final long serialVersionUID = 1L;
    
    private List<String> allowedOperations;

    public List<String> getAllowedOperations() {
        return allowedOperations;
    }

    public void setAllowedOperations(List<String> allowedOperations) {
        this.allowedOperations = allowedOperations;
    }
}

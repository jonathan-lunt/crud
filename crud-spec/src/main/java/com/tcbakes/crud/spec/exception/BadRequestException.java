package com.tcbakes.crud.spec.exception;

import javax.ws.rs.core.Response.Status;

/**
 * A required argument is not provided or is otherwise incorrect. This is
 * equivalent to a HTTP 400 response code
 * 
 * @see javax.ws.rs.core.Response.Status#BAD_REQUEST
 * @author Tristan Baker
 * 
 */
public class BadRequestException extends AbstractSpecException {

    private static final long serialVersionUID = 1L;

    public BadRequestException() {
        super();
    }

    public BadRequestException(String msg) {
        super(msg);
    }
    public BadRequestException(String msg, Throwable cause) {
        super(msg, cause);
    }

    @Override
    public Status getHttpStatusCode() {
        return Status.BAD_REQUEST;
    }

}
